#!/bin/sh
set -e

RV=0

if nm --print-size --size-sort --radix=d "$1" | grep 4core3fmt5float; then
  echo "FATAL: Float formatting found" >&2
  RV=1
fi

exit $RV
