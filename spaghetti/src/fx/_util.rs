use palette::{Hsv, RgbHue};

#[inline(always)]
pub fn u8_to_float(v: u8) -> f32 {
    v as f32 / 255.
}

pub fn fire_color_scale(value: f32) -> Hsv {
    Hsv::new_srgb_const(RgbHue::new(value * 60.0), 1.0, libm::sqrtf(value))
}
