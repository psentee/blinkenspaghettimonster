use super::_prelude::*;

// TODO: parameterize?
const CRAWL_LEN: isize = {
    // should be odd for symmetry
    const HALF: isize = (Coords::N as isize / 2) | 1;
    if HALF < 255 {
        HALF
    } else {
        255
    }
};
const HALF_CRAWL: isize = (CRAWL_LEN / 2) as _;

pub async fn crawl(sh: &mut impl Show) {
    for start in (-CRAWL_LEN)..(Coords::N as isize) {
        for (i, px) in sh.frame_mut().iter_mut().enumerate() {
            let pos = i as isize - start;

            // lets outside the crawl are off
            *px = if (0..CRAWL_LEN as isize).contains(&pos) {
                // brightness (V) just increases linearly for the first
                // half of the crawl, and then decreases linearly for the
                // second half. TODO: smoother function (sine? bell
                // curve?)
                let unscaled_v = HALF_CRAWL - HALF_CRAWL.abs_diff(pos) as isize;

                const RAINBOW_RANGE: usize = 23 * 17;
                palette::Hsv::new_srgb_const(
                    // hue is random rainbow based on the frame number
                    ((i.wrapping_shl(2).wrapping_add(start as usize) % (RAINBOW_RANGE + 1)) as f32
                        / (RAINBOW_RANGE as f32 / 360.))
                        .into(),
                    1.0,
                    unscaled_v as f32 / HALF_CRAWL as f32,
                )
                .into_color()
            } else {
                crate::colors::BLACK
            };
        }

        sh.frame_done().await;
    }
}
