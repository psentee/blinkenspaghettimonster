use super::_prelude::*;

const RADIUS: usize = 14; // FIXME: this should be in coords

pub async fn zoomies(sh: &mut impl Show) {
    let origin = Foint::new(128., 128.);
    loop {
        let mut finish = false;

        let length = sh.rng().gen_range(16..48);

        for frame_num in 0..(length + RADIUS) {
            let frame_num = frame_num as f32;
            for i in 0..Coords::N {
                let pt = COORDS.coords[i].to_f32();
                let vec = pt - origin;
                let dist = frame_num - libm::sqrtf(vec.length());

                if dist < 0. || dist > length as f32 {
                    sh.frame_mut()[i] = colors::BLACK;
                } else {
                    let hue = dist / length as f32;
                    sh.frame_mut()[i] = Hsv::new_srgb(hue * 360., 1.0, 1.0).into_color();
                }
            }
            finish |= sh.frame_done().await.should_finish();
        }

        if finish {
            break;
        }
    }
}
