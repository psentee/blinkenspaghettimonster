use core::ops::Range;

use super::_prelude::*;

struct Firework {
    start: usize,
    end: usize,
    left: isize,
    right: usize,
    wait: f32,
    ctr: usize,
}

impl Firework {
    const FADE: f32 = 0.02;
    const SLOWDOWN: f32 = 1.06;

    fn new(strand: &Range<usize>) -> Self {
        Self {
            start: strand.start,
            end: strand.end,
            left: ((strand.start + strand.end) / 2) as isize,
            right: (strand.start + strand.end) / 2,
            wait: 1.0,
            ctr: 0,
        }
    }

    fn step(&mut self, sh: &mut impl Show) {
        let fr = sh.frame_mut();
        for j in ((self.left + 1) as usize)..(self.right - 1) {
            fr[j].darken_assign(Self::FADE);
        }

        if self.ctr > 0 {
            self.ctr -= 1;
            return;
        }

        let color_v =
            1.0 - 0.8 * ((self.right as isize - self.left) as f32 / (self.end - self.start) as f32);
        let color: Srgb = super::_util::fire_color_scale(color_v).into_color();

        if self.left >= self.start as isize {
            fr[self.left as usize] = color;
        }
        if self.right < self.end {
            fr[self.right] = color;
        }
        self.left -= 1;
        self.right += 1;

        if self.left < self.start as isize && self.right >= self.end {
            self.ctr = usize::MAX;
        } else {
            self.wait *= Self::SLOWDOWN;
            self.ctr = self.wait as usize - 1;
        }
    }

    fn done(&self, sh: &impl Show) -> bool {
        // we finished animation (ctr counting down from usize::MAX), and…
        self.ctr > usize::MAX / 2
            && (
                // …either all pixels have faded to basically black, …
                (self.start..self.end).all(|i| {
                let px = sh.frame()[i];
                px.red + px.green + px.blue < 0.001
                }) ||          // …or it's 150 frames (50 seconds) since we started fading down
                    self.ctr < usize::MAX - 150
            )
    }
}

pub async fn fireworks(sh: &mut impl Show) {
    sh.frame_mut().clear();

    let mut fws: heapless::Vec<Firework, { Coords::N_STRANDS }> =
        COORDS.strands.iter().map(Firework::new).collect();
    fws.shuffle(sh.rng());

    let mut i: usize = 1;
    loop {
        if sh.rng().gen_bool(0.025) {
            i += sh.rng().gen_range(1..3);
        }
        for fw in fws.iter_mut().take(i) {
            fw.step(sh);
        }
        sh.frame_done().await;
        if fws.iter().all(|fw| fw.done(sh)) {
            break;
        }
    }
}
