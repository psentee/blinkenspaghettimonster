pub(crate) use palette::{Darken, DarkenAssign, Hsv, IntoColor, Srgb};
pub(crate) use rand::prelude::*;

pub(crate) use crate::{
    colors,
    coords::{Coords, COORDS},
    show::{Duration, Frame, Show},
    // Point2D,
};

pub(crate) type Foint = euclid::default::Point2D<f32>;
pub(crate) type Fector = euclid::default::Vector2D<f32>;
pub(crate) type Frect = euclid::default::Rect<f32>;
pub(crate) type Angle = euclid::Angle<f32>;
