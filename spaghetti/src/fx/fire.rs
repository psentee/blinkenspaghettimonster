use core::ops::Range;

use alloc::boxed::Box;

use super::_prelude::*;

struct Fire {
    buf: Box<[[u8; 128]; 130]>,
    extinguishing: bool,
    bottom: Range<usize>,
}

impl Fire {
    fn new(rng: &mut impl Rng) -> Self {
        let mut rv = Self {
            buf: unsafe {
                // Safety: it's an array of u8, zeroes are safe
                Box::new_zeroed().assume_init()
            },
            extinguishing: false,
            bottom: 63..64,
        };
        rv.fill_bottom(rng);
        rv
    }

    fn extinguish(&mut self) {
        if !self.extinguishing {
            log::debug!("Putting out the fire");
        }
        self.extinguishing = true;
    }

    fn fill_bottom(&mut self, rng: &mut impl Rng) {
        let delta_range: Range<isize> = if self.extinguishing {
            -10..0
        } else {
            match self.bottom.len() {
                0..=15 => 1..8,
                16..=63 => -2..6,
                64..=95 => -4..4,
                96.. => -6..2,
                _ => panic!("CAN'T HAPPEN"),
            }
        };

        let mid = ((self.bottom.start + self.bottom.end) / 2) as isize;
        let new_start = (self.bottom.start as isize - rng.gen_range(delta_range.clone()))
            .clamp(0, mid) as usize;
        let new_end = (self.bottom.end as isize + rng.gen_range(delta_range.clone()))
            .clamp(mid, 128) as usize;
        self.bottom = new_start..new_end;

        let mut bytes: [u8; 128] = [0; 128];
        rng.fill(&mut bytes[self.bottom.clone()]);
        for i in 0..2usize {
            for j in 0..128usize {
                self.buf[128 + i][j] = (bytes[j] << (i * 4)) & 0xF0;
            }
        }
    }

    fn calculate(&mut self, rng: &mut impl Rng) -> bool {
        if rng.gen_bool(0.1) {
            self.fill_bottom(rng);
        }

        let mut flaming: bool = false;
        for i in 0..128usize {
            for j in 0..128usize {
                let i1 = i + 1;
                let value: u16 = (self.buf[i1][j] as u16
                    + self.buf[i1][j.wrapping_sub(1) & 0x7F] as u16
                    + self.buf[i1][(j + 1) & 0x7F] as u16
                    + self.buf[i1 + 1][j] as u16)
                    * 64
                    / 257;
                self.buf[i][j] = value as u8;
                flaming = flaming || value != 0;
            }
        }

        flaming
    }

    fn render(&self, fr: &mut Frame) {
        fr.iter_mut()
            .zip(COORDS.coords.iter())
            .for_each(|(px, pos)| {
                *px = super::_util::fire_color_scale(super::_util::u8_to_float(
                    self.buf[(pos.y >> 1) as usize][(pos.x >> 1) as usize],
                ))
                .into_color();
            });
    }
}

pub async fn fire(sh: &mut impl Show) {
    let mut fire = Fire::new(sh.rng());

    while fire.calculate(sh.rng()) {
        fire.render(sh.frame_mut());
        if sh.frame_done().await.should_finish() {
            fire.extinguish();
        }
    }
}
