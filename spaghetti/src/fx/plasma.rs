use super::_prelude::*;
use crate::util::{cos8, sin8};

fn plasma_fn(i: u8, x: u8, y: u8) -> u8 {
    let xs = sin8(x.wrapping_add(i)) as u16;
    let yc = cos8(y.wrapping_add(i)) as u16;
    ((xs + yc) >> 1) as u8
}

pub async fn plasma(sh: &mut impl Show) {
    for frame in 0usize.. {
        for (i, &euclid::Point2D { x, y, .. }) in COORDS.coords.iter().enumerate() {
            let plv = plasma_fn(frame as u8, x, y);

            // FIXME: make calculations in floats
            let hsv = palette::Hsv::new_srgb(
                plv.wrapping_add((frame >> 2) as u8).wrapping_add(x),
                255,
                cos8(plv),
            )
            .into_format();
            let rgb: palette::Srgb = hsv.into_color();
            let fade = if frame >= 0x80 {
                1.0
            } else {
                frame as f32 / 128.
            };

            sh.frame_mut()[i] = rgb * fade;
        }

        if sh.frame_done().await.should_finish() {
            break;
        }
    }
    super::transition::fade(0.2, sh).await;
}
