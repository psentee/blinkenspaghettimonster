#![feature(const_maybe_uninit_zeroed, new_uninit)]
#![cfg_attr(not(test), no_std)]

extern crate alloc;

mod framebuffer;
pub mod fx;
mod show;
mod util;

#[allow(dead_code)]
mod coords {
    pub type Point2D = euclid::default::Point2D<u8>;

    include!(concat!(env!("OUT_DIR"), "/coords.rs"));
}

#[allow(dead_code)]
pub mod colors {
    include!(concat!(env!("OUT_DIR"), "/colors.rs"));
}

pub use coords::*;
pub use framebuffer::*;
pub use show::*;
