use anyhow::Result;
use quote::quote;

use crate::config::Config;

const ALMOST_256: f32 = 256.0 - f32::EPSILON;

fn to_u8(coord: f32) -> u8 {
    assert!((0.0..=1.0).contains(&coord));
    (coord * ALMOST_256).floor() as _
}

pub fn generate(config: &Config) -> Result<String> {
    let Config { coords, strands } = config;
    let n_leds = coords.len();

    let max_consecutive_dist = config.max_consecutive_dist();
    let max_consecutive_sqdist = (max_consecutive_dist * ALMOST_256).powi(2).floor() as u16;

    let n_strands = strands.len();
    let max_strand_len = strands.iter().map(|strand| strand.len()).max();
    let strand_tokens = strands.iter().map(|range| {
        let std::ops::Range { end, start } = range;
        quote! { #start..#end }
    });

    let neighbours = config.neighbours();
    let max_neighbours = neighbours.iter().map(|nn| nn.len()).max();
    let neighbour_tokens = neighbours.iter().map(|nn| quote! { &[#(#nn),*] });

    let coords_tks = coords.iter().map(|pt| {
        let x = to_u8(pt.x);
        let y = to_u8(pt.y);
        quote! { Point2D::new(#x, #y) }
    });
    let coords_array_tks = quote!([#(#coords_tks),*]);

    let module = quote! {
        pub struct Coords {
            pub coords: [Point2D; #n_leds],
            pub max_consequtive_sqdist: u16,
            pub neighbours: [&'static[usize]; #n_leds],
            pub strands: [core::ops::Range<usize>; #n_strands],
        }

        impl Coords {
            pub const N: usize = #n_leds;
            pub const N_STRANDS: usize = #n_strands;
            pub const MAX_STRAND_LEN: usize = #max_strand_len;
            pub const MAX_NEIGHBOURS: usize = #max_neighbours;
        }

        pub static COORDS: Coords = Coords {
            coords: #coords_array_tks,
            max_consequtive_sqdist: #max_consecutive_sqdist,
            neighbours: [#(#neighbour_tokens),*],
            strands: [#(#strand_tokens),*],
        };
    };
    Ok(prettyplease::unparse(&syn::parse2(module)?))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_to_u8() {
        for (before, after) in [
            (0.0, 0),
            (1.0, 255),
            (1. / 257., 0),
            (0.999, 255),
            (0.5, 128),
        ]
        .into_iter()
        {
            assert_eq!(to_u8(before), after);
        }
    }
}
