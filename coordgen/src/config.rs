use std::{collections::HashSet, fs::File, ops::Range, path::PathBuf};

use anyhow::{Context, Result};
use lyon_geom::Point;

mod conf_file {
    use std::{fs::File, path::PathBuf};

    use anyhow::Result;

    #[derive(Debug, Default, serde::Serialize, serde::Deserialize)]
    #[serde(default)]
    pub(super) struct Config {
        pub(super) coords: PathBuf,
        pub(super) strands: Vec<usize>,
    }

    impl Config {
        pub(super) fn load<P: AsRef<std::path::Path>>(path: P) -> Result<Self> {
            Ok(ron::de::from_reader::<_, Self>(File::open(path.as_ref())?)?)
        }
    }
}

#[derive(Debug)]
pub struct Config {
    pub coords: Vec<Point<f32>>,
    pub strands: Vec<Range<usize>>,
}

impl Config {
    pub fn load<P: AsRef<std::path::Path>>(path: P) -> Result<Self> {
        let path = PathBuf::from(path.as_ref());
        let conf = dbg!(conf_file::Config::load(&path)?);

        let coords_path = path.parent().context("parent")?.join(conf.coords);
        log::info!("Loading coordinates from {}", coords_path.display());
        let coord_pairs: Vec<(f32, f32)> = ron::de::from_reader(File::open(&coords_path)?)?;
        let n = coord_pairs.len();

        let mut strands: Vec<Range<usize>> = Vec::new();
        let mut cursor: usize = 0;
        for strand in conf.strands.iter().cloned() {
            let next = cursor + strand;
            strands.push(cursor..next);
            cursor = next;
        }
        match cursor.cmp(&n) {
            std::cmp::Ordering::Less => {
                let missing_strand = cursor..n;
                log::warn!(
                    "Strands specified only {}, adding a missing strand of {}: {:?}",
                    cursor,
                    n - cursor,
                    missing_strand
                );
                strands.push(missing_strand);
            }
            std::cmp::Ordering::Equal => {
                log::debug!("Strands are exactly {} long, nice!", cursor);
            }
            std::cmp::Ordering::Greater => {
                anyhow::bail!("Strands are {}, but we only have {} points", cursor, n);
            }
        }

        Ok(Self {
            coords: coord_pairs
                .into_iter()
                .map(|(x, y)| Point::new(x, y))
                .collect(),
            strands,
        })
    }

    // TODO: memoize?
    pub fn max_consecutive_dist(&self) -> f32 {
        self.strands
            .iter()
            .flat_map(|strand| {
                shrink_range(strand).map(|i| {
                    let pt1 = self.coords[i];
                    let pt2 = self.coords[i + 1];
                    let dist = pt1.distance_to(pt2);
                    if dist > 0.05 {
                        dbg!((i, i + 1, dist));
                    }
                    dist
                })
            })
            .max_by(|a, b| a.total_cmp(b))
            .unwrap()
    }

    fn strand_pairs(&self) -> impl Iterator<Item = (Range<usize>, Range<usize>)> + '_ {
        self.strands.iter().enumerate().flat_map(|(i, strand1)| {
            self.strands
                .iter()
                .skip(i + 1)
                .map(|strand2| (strand1.clone(), strand2.clone()))
        })
    }

    fn intersections(&self) -> impl Iterator<Item = (usize, usize)> + '_ {
        self.strand_pairs()
            // cross product of segments between consecutive LEDs:
            .flat_map(move |(strand1, strand2)| {
                let strand1 = shrink_range(&strand1);
                let strand2 = shrink_range(&strand2);
                strand1
                    .clone()
                    .map(|i| {
                        (
                            i,
                            lyon_geom::LineSegment {
                                from: self.coords[i],
                                to: self.coords[i + 1],
                            },
                        )
                    })
                    .flat_map(move |(i, seg)| {
                        strand2.clone().map(move |j| {
                            (
                                i,
                                seg,
                                j,
                                lyon_geom::LineSegment {
                                    from: self.coords[j],
                                    to: self.coords[j + 1],
                                },
                            )
                        })
                    })
            })
            // actual intersections:
            .filter_map(|(i, seg1, j, seg2)| {
                if seg1.intersects(&seg2) {
                    Some((i, j))
                } else {
                    None
                }
            })
    }

    fn jumps(&self) -> impl Iterator<Item = (usize, usize)> + '_ {
        let jump_distance = self.max_consecutive_dist() * 1.5;
        self.strand_pairs().filter_map(move |(strand1, strand2)| {
            let (i, j, dist) = strand1
                .flat_map(move |i| {
                    strand2
                        .clone()
                        .map(move |j| (i, j, self.coords[i].distance_to(self.coords[j])))
                })
                .min_by(|(_i1, _j1, dist1), (_i2, _j2, dist2)| dist1.total_cmp(dist2))
                .unwrap();

            if dist < jump_distance {
                log::debug!("Jump {}-{} (distance={})", i, j, dist);
                Some((i, j))
            } else {
                None
            }
        })
    }

    pub fn neighbours(&self) -> Vec<Vec<usize>> {
        // Start with just neighbours on the same strand
        let starts: HashSet<usize> = self.strands.iter().map(|s| s.start).collect();
        let ends: HashSet<usize> = self.strands.iter().map(|s| s.end - 1).collect();
        let mut neighbours: Vec<HashSet<usize>> = (0..self.coords.len())
            .map(|i| {
                let mut ns: HashSet<usize> = HashSet::new();
                if !starts.contains(&i) {
                    ns.insert(i - 1);
                }
                if !ends.contains(&i) {
                    ns.insert(i + 1);
                }
                ns
            })
            .collect();

        // Add intersections
        for (pt1, pt2) in self.intersections() {
            neighbours[pt1].extend(&[pt2, pt2 + 1]);
            neighbours[pt1 + 1].extend(&[pt2, pt2 + 1]);
            neighbours[pt2].extend(&[pt1, pt1 + 1]);
            neighbours[pt2 + 1].extend(&[pt1, pt1 + 1]);
        }

        // Jumps between strands
        for (pt1, pt2) in self.jumps() {
            neighbours[pt1].insert(pt2);
            neighbours[pt2].insert(pt1);
        }

        neighbours
            .into_iter()
            .map(|ns| {
                let mut nv: Vec<usize> = ns.into_iter().collect();
                nv.sort();
                nv
            })
            .collect()
    }
}

fn shrink_range(strand1: &Range<usize>) -> Range<usize> {
    strand1.start..(strand1.end - 1)
}
