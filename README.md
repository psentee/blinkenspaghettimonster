BlinkenSpaghettiMonster
=======================

A 2D blinking display from LEDs arranged in an irregular pattern (not
on a grid) that still displays an image according to actual positions
of LEDs.

This repo is my own notes (and work in progress) rather than something
intended for others, but maybe it will be somehow useful.

Hardware
--------

 - [Teensy LC](https://www.pjrc.com/teensy/teensyLC.html)
 - 10 meters of
   [WS2821B](https://cdn-shop.adafruit.com/datasheets/WS2812B.pdf) LED
   stripe (60LED/m, 18W/m), cut into pieces to arrange randomly and to
   distribute power in a "star topology"
 - 600W ATX power supply, with 50A / 200W capacity on the 5V line

### Teensy CPU

MKL26Z64VFT4 (Cortex-M0+ 48MHz)
https://www.digikey.com/product-detail/en/nxp-usa-inc/MKL26Z64VFT4/MKL26Z64VFT4-ND/4234818
https://www.nxp.com/part/MKL26Z64VFT4
https://www.nxp.com/docs/en/data-sheet/KL26P64M48SF5.pdf
https://www.pjrc.com/teensy/KL26P64M48SF5.pdf
https://developer.arm.com/ip-products/processors/cortex-m/cortex-m0-plus
https://developer.arm.com/embedded/cmsis/cmsis-packs/devices/NXP/MKL26Z64XXX4

Software
--------

All is built on FreeBSD (at the first commit to this repo: 13-CURRENT,
r350583, amd64 arch) because it's what I use.

Makefiles are for FreeBSD's bmake (but you might make them work using
a [portable version](http://crufty.net/help/sjg/bmake.html)). Patches
with portability fixes are welcome; ones that replace toolchain with
something Linux-specific (or bmake with GNU make), not that much.

### Third party software

Software not found in FreeBSD ports is imported in the `vendor/`
subdirectory. Each vendor subdirectory is a Git subtree.

 - [teensy\_loader\_cli](https://github.com/PaulStoffregen/teensy_loader_cli),
   the CLI loader for Teensy

### Used toolchain

Following tools outside of Freebsd 13 base system and contents of this
repo are needed:

 - [Rust](https://www.rust-lang.org/) nightly (from
   [rustup](https://rustup.rs/))

        rustup toolchain install nightly
        rustup default nightly
        rustup target add thumbv6m-none-eabi
        rustup component add llvm-tools-preview

These are useful:

 - `arm-none-eabi-binutils` package for debugging with `objdump -d`

Bibliography
------------

 - https://github.com/roseengineering/teensylc-baremetal (uses gcc)
 - https://sushihangover.github.io/arm-bare-metal-comparing-llvm-to-arm-gcc/
   (clang invocations, but not lld)
 - https://github.com/PaulStoffregen/cores/blob/master/teensy3/mkl26z64.ld
   & mk20dx128.c
 - https://josh.robsonchase.com/embedded-bootstrapping/ about bricking
   teensy by overwriting flashconfig
 - https://www.pjrc.com/teensy/schematic_lc.gif
 - https://github.com/PaulStoffregen/cores (also `make -C doc teensy-lc`)
 - https://github.com/apmorton/teensy-template

### Rust?

 - https://github.com/rust-embedded/cortex-m-quickstart
 - https://rust-embedded.github.io/book/
 - https://branan.github.io/teensy/
