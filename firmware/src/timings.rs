#[derive(Clone, Copy)]
pub struct Percentage(u32, u32, u8);

impl Percentage {
    pub fn new(micros: u32, part: u32, whole: u32) -> Self {
        let hundreds = (part / whole) * 100;
        let part = part % whole;
        let frac = part * 100 / (whole / 100);
        Self(micros, hundreds + frac / 100, (frac % 100) as u8)
    }
}

impl core::fmt::Display for Percentage {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{}μs ({}.{:02}%)", self.0, self.1, self.2)
    }
}

pub type Duration = fugit::Duration<u32, 1, { bsp::board::ARM_FREQUENCY }>;
pub type Instant = fugit::Instant<u32, 1, { bsp::board::ARM_FREQUENCY }>;

pub type DurationPERCLK = fugit::Duration<u32, 1, { bsp::board::PERCLK_FREQUENCY }>;

pub const FPS: fugit::HertzU32 = fugit::HertzU32::from_raw(30);
pub const FRAME_DURATION: Duration = Duration::from_rate(FPS);

pub trait InstantExt {
    fn now() -> Self;
}

pub trait DurationExt {
    fn since(since_when: Instant) -> Self;
    fn as_frame_percentage(&self) -> Percentage;
    fn measure<R>(inner: impl FnOnce() -> R) -> (Self, R)
    where
        Self: Sized;
}

impl InstantExt for Instant {
    fn now() -> Self {
        Self::from_ticks(cortex_m::peripheral::DWT::cycle_count())
    }
}

impl DurationExt for Duration {
    fn since(since_when: Instant) -> Self {
        // Use raw wrapping_sub because Instant's Sub uses
        // checked_sub, and cycle counter overflows every 7.16 seconds.
        Self::from_ticks(Instant::now().ticks().wrapping_sub(since_when.ticks()))
    }

    fn as_frame_percentage(&self) -> Percentage {
        Percentage::new(self.to_micros(), self.ticks(), FRAME_DURATION.ticks())
    }

    fn measure<R>(inner: impl FnOnce() -> R) -> (Self, R) {
        let start = Instant::now();
        let rv = inner();
        (Self::since(start), rv)
    }
}
