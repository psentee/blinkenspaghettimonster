pub struct Trng(bsp::hal::trng::Trng);

impl Trng {
    pub const fn new(trng: bsp::hal::trng::Trng) -> Self {
        Self(trng)
    }

    fn iter_bytes(&mut self) -> impl Iterator<Item = Result<u8, ()>> + '_ {
        core::iter::from_fn(|| {
            Some(match nb::block!(self.0.next_u32()) {
                Ok(word) => [
                    Ok((word & 255) as u8),
                    Ok(((word >> 8) & 255) as u8),
                    Ok(((word >> 16) & 255) as u8),
                    Ok(((word >> 24) & 255) as u8),
                ],
                Err(err) => {
                    unsafe {
                        crate::logging::debug_value(err);
                    }
                    [Err(()), Err(()), Err(()), Err(())]
                }
            })
        })
        .flatten()
    }
}

impl rand::RngCore for Trng {
    fn next_u32(&mut self) -> u32 {
        self.0.next_u32().unwrap()
    }

    fn next_u64(&mut self) -> u64 {
        ((self.next_u32() as u64) << 32) + self.next_u32() as u64
    }

    fn fill_bytes(&mut self, dest: &mut [u8]) {
        self.try_fill_bytes(dest).unwrap();
    }

    fn try_fill_bytes(&mut self, dest: &mut [u8]) -> Result<(), rand::Error> {
        for (d, b) in dest.iter_mut().zip(self.iter_bytes()) {
            *d = b.map_err(|_| {
                rand::Error::from(unsafe { core::num::NonZeroU32::new_unchecked(23) })
            })?;
        }
        Ok(())
    }
}
