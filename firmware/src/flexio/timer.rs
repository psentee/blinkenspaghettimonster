#![allow(dead_code)]

use imxrt_ral as ral;
use ral::flexio;

use super::Polarity;

#[derive(Copy, Clone)]
pub enum Mode {
    /// Disabled
    Disabled,

    /// 8-bit Baud Counter Mode (bits, divider)
    Dual8BitBaud(u8, u8),

    /// 8-bit High PWM Mode (high period, low period)
    Dual8BitPWM(u8, u8),

    /// 16-bit Counter Mode (raw TIMCMP_CMP value, as it is treated differently depending on mode)
    Single16bitCounter(u16),
}

impl Mode {
    #[inline(always)]
    const fn timcmp(self) -> u32 {
        match self {
            Self::Disabled => 0,

            // TODO: check for validity of bits/divider (either via types, or in runtime)
            Self::Dual8BitBaud(bits, divider) => {
                // The lower 8-bits configure the baud rate divider equal to (CMP[7:0] + 1) * 2.
                ((divider as u32 / 2) - 1)

                // The upper 8-bits configure the number of bits in each word equal to (CMP[15:8] + 1) / 2.
                    | ((bits as u32 * 2) - 1) << 8
            }

            // the lower 8-bits configure the high period of the
            // output to (CMP[7:0] + 1) and the upper 8-bits configure
            // the low period of the output to (CMP[15:8] + 1).
            Self::Dual8BitPWM(high, low) => (low as u32 - 1) << 8 | (high as u32 - 1),

            // The compare value can be used to generate the baud rate
            // divider (if shift clock source is timer output) to
            // equal (CMP[15:0] + 1) * 2. When the shift clock source
            // is a pin or trigger input, the compare register is used
            // to set the number of bits in each word equal to
            // (CMP[15:0] + 1) / 2.
            //
            // TODO: meaningful calculation like above? It would need
            //       to depend on shifter config somehow.
            Self::Single16bitCounter(cmp) => cmp as u32,
        }
    }

    #[inline(always)]
    const fn timod(self) -> u32 {
        use flexio::TIMCTL::TIMOD::RW::*;
        match self {
            Self::Disabled => TIMOD_0,
            Self::Dual8BitBaud(_, _) => TIMOD_1,
            Self::Dual8BitPWM(_, _) => TIMOD_2,
            Self::Single16bitCounter(_) => TIMOD_3,
        }
    }
}

#[derive(Copy, Clone)]
pub enum Trigger {
    /// External trigger
    External(u8, Polarity),

    /// Pin input
    Pin(u8, Polarity),

    /// Shifter status
    Shifter(u8, Polarity),

    /// Timer trigger output
    Timer(u8, Polarity),
}

impl Trigger {
    const fn trgsel(self) -> u32 {
        (match self {
            Self::External(trigger, _) => trigger, // 6 bits? trigger=0 in param
            Self::Pin(pin, _) => pin << 1,
            Self::Shifter(shifter, _) => shifter << 2 | 1, // 4 bits
            Self::Timer(timer, _) => timer << 2 | 3,       // 4 bits
        }) as _
    }

    const fn polarity(self) -> Polarity {
        match self {
            Self::External(_, polarity)
            | Self::Pin(_, polarity)
            | Self::Shifter(_, polarity)
            | Self::Timer(_, polarity) => polarity,
        }
    }

    const fn trgpol(self) -> u32 {
        use flexio::TIMCTL::TRGPOL::RW::*;
        match self.polarity() {
            Polarity::ActiveHigh => TRGPOL_0,
            Polarity::ActiveLow => TRGPOL_1,
        }
    }

    const fn trgsrc(self) -> u32 {
        use flexio::TIMCTL::TRGSRC::RW::*;
        match self {
            Self::External(_, _) => TRGSRC_0,
            _ => TRGSRC_1,
        }
    }
}

// The pin configuration for each timer and shifter can be configured
// to use any FlexIO pin with either polarity. Each timer and shifter
// can be configured as an input, output data, output enable or
// bidirectional output. A pin configured for output enable can be
// used as an open drain (with inverted polarity, since the output
// enable assertion would cause logic zero to be output on the pin) or
// to control the enable on the bidirectional output. Any timer or
// shifter could be configured to control the output enable for a pin
// where the bidirectional output data is driven by another timer or
// shifter.

#[derive(Copy, Clone)]
pub enum Pin {
    /// Timer pin output disabled
    Disabled,

    /// Timer pin open drain or bidirectional output enable
    OpenDrain(u8, Polarity), // is polarity needed for OE (or OD)? Can we make an alias here?

    /// Timer pin bidirectional output data
    BidirectionalOutputData(u8, Polarity),

    /// Timer pin output
    Output(u8, Polarity),
}

impl Pin {
    const fn pinsel(self) -> u32 {
        match self {
            Pin::Disabled => 0,
            Pin::OpenDrain(pin, _) | Pin::BidirectionalOutputData(pin, _) | Pin::Output(pin, _) => {
                pin as u32
            }
        }
    }

    const fn pincfg(self) -> u32 {
        use flexio::TIMCTL::PINCFG::RW::*;
        match self {
            Pin::Disabled => PINCFG_0,
            Pin::OpenDrain(_, _) => PINCFG_1,
            Pin::BidirectionalOutputData(_, _) => PINCFG_2,
            Pin::Output(_, _) => PINCFG_3,
        }
    }

    const fn polarity(self) -> Polarity {
        match self {
            Pin::Disabled => Polarity::ActiveHigh,
            Pin::OpenDrain(_, polarity)
            | Pin::BidirectionalOutputData(_, polarity)
            | Pin::Output(_, polarity) => polarity,
        }
    }

    const fn pinpol(self) -> u32 {
        use flexio::TIMCTL::PINPOL::RW::*;
        match self.polarity() {
            Polarity::ActiveHigh => PINPOL_0,
            Polarity::ActiveLow => PINPOL_1,
        }
    }
}

#[derive(Copy, Clone)]
pub enum Output {
    /// Timer output is logic one when enabled and is not affected by timer reset (default)
    HighWhenEnabled,

    /// Timer output is logic zero when enabled and is not affected by timer reset
    LowWhenEnabled,

    /// Timer output is logic one when enabled and on timer reset
    HighWhenEnabledAndReset,

    /// Timer output is logic zero when enabled and on timer reset
    LowWhenEnabledAndReset,
}

impl Output {
    const fn timout(self) -> u32 {
        use flexio::TIMCFG::TIMOUT::RW::*;
        match self {
            Output::HighWhenEnabled => TIMOUT_0,
            Output::LowWhenEnabled => TIMOUT_1,
            Output::HighWhenEnabledAndReset => TIMOUT_2,
            Output::LowWhenEnabledAndReset => TIMOUT_3,
        }
    }
}

/// Configures the source of the Timer decrement and the source of the Shift clock.
#[derive(Copy, Clone)]
pub enum Decrement {
    /// Decrement counter on FlexIO clock, Shift clock equals Timer output. (default)
    Clock,

    /// Decrement counter on Trigger input (both edges), Shift clock equals Timer output.
    Trigger,

    /// Decrement counter on Pin input (both edges), Shift clock equals Pin input.
    PinShift,

    /// Decrement counter on Trigger input (both edges), Shift clock equals Trigger input.
    TriggerShift,
}

impl Decrement {
    const fn timdec(self) -> u32 {
        use flexio::TIMCFG::TIMDEC::RW::*;
        match self {
            Decrement::Clock => TIMDEC_0,
            Decrement::Trigger => TIMDEC_1,
            Decrement::PinShift => TIMDEC_2,
            Decrement::TriggerShift => TIMDEC_3,
        }
    }
}

/// Configures the condition that causes the timer counter (and
/// optionally the timer output) to be reset. In 8-bit counter mode,
/// the timer reset will only reset the lower 8-bits that configure
/// the baud rate. In all other modes, the timer reset will reset the
/// full 16-bits of the counter.
#[derive(Copy, Clone)]
pub enum Reset {
    /// Timer never reset (default)
    Never,

    /// Timer reset on Timer Pin equal to Timer Output
    PinEqualsOutput,

    /// Timer reset on Timer Trigger equal to Timer Output
    TriggerEqualsOutput,

    /// Timer reset on Timer Pin rising edge
    PinRisingEdge,

    /// Timer reset on Trigger rising edge
    TriggerRisingEdge,

    /// Timer reset on Trigger rising or falling edge
    TriggerAnyEdge,
}

impl Reset {
    const fn timrst(self) -> u32 {
        use flexio::TIMCFG::TIMRST::RW::*;
        match self {
            Reset::Never => TIMRST_0,
            Reset::PinEqualsOutput => TIMRST_2,
            Reset::TriggerEqualsOutput => TIMRST_3,
            Reset::PinRisingEdge => TIMRST_4,
            Reset::TriggerRisingEdge => TIMRST_6,
            Reset::TriggerAnyEdge => TIMRST_7,
        }
    }
}

/// Configures the condition that causes the Timer to be disabled and stop decrementing.
#[derive(Copy, Clone)]
pub enum Disable {
    /// Timer never disabled (default)
    Never,

    /// Timer disabled on Timer N-1 disable
    PreviousTimer,

    /// Timer disabled on Timer compare
    Compare,

    /// Timer disabled on Timer compare and Trigger Low
    CompareTriggerLow,

    /// Timer disabled on Pin rising or falling edge
    PinEdge,

    /// Timer disabled on Pin rising or falling edge provided Trigger is high
    PinEdgeTriggerHigh,

    /// Timer disabled on Trigger falling edge
    TriggerFalling,
}

impl Disable {
    const fn timdis(self) -> u32 {
        use flexio::TIMCFG::TIMDIS::RW::*;
        match self {
            Disable::Never => TIMDIS_0,
            Disable::PreviousTimer => TIMDIS_1,
            Disable::Compare => TIMDIS_2,
            Disable::CompareTriggerLow => TIMDIS_3,
            Disable::PinEdge => TIMDIS_4,
            Disable::PinEdgeTriggerHigh => TIMDIS_5,
            Disable::TriggerFalling => TIMDIS_6,
        }
    }
}

/// Timer Enable
/// Configures the condition that causes the Timer to be enabled and start decrementing.
#[derive(Copy, Clone)]
pub enum Enable {
    /// Timer always enabled (default)
    Always,

    /// Timer enabled on Timer N-1 enable
    PreviousTimer,

    /// Timer enabled on Trigger high
    Trigger,

    /// Timer enabled on Trigger high and Pin high
    TriggerAndPin,

    /// Timer enabled on Pin rising edge
    PinRising,

    /// Timer enabled on Pin rising edge and Trigger high
    PinRisingTriggerHigh,

    /// Timer enabled on Trigger rising edge
    TriggerRising,

    /// Timer enabled on Trigger rising or falling edge
    TriggerEdge,
}

impl Enable {
    const fn timena(self) -> u32 {
        use flexio::TIMCFG::TIMENA::RW::*;
        match self {
            Enable::Always => TIMENA_0,
            Enable::PreviousTimer => TIMENA_1,
            Enable::Trigger => TIMENA_2,
            Enable::TriggerAndPin => TIMENA_3,
            Enable::PinRising => TIMENA_4,
            Enable::PinRisingTriggerHigh => TIMENA_5,
            Enable::TriggerRising => TIMENA_6,
            Enable::TriggerEdge => TIMENA_7,
        }
    }
}

/// The stop bit can be added on a timer compare (between each word) or on a timer disable. When stop bit
/// is enabled, configured shifters will output the contents of the stop bit when the timer is disabled. When
/// stop bit is enabled on timer disable, the timer remains disabled until the next rising edge of the shift clock.
/// If configured for both timer compare and timer disable, only one stop bit is inserted on timer disable.
#[derive(Copy, Clone)]
pub enum StopBit {
    /// Stop bit disabled (default)
    Disabled,

    /// Stop bit is enabled on timer compare
    OnCompare,

    /// Stop bit is enabled on timer disable
    OnDisable,

    /// Stop bit is enabled on timer compare and timer disable
    OnCompareAndDisable,
}

impl StopBit {
    const fn tstop(self) -> u32 {
        use flexio::TIMCFG::TSTOP::RW::*;
        match self {
            StopBit::Disabled => TSTOP_0,
            StopBit::OnCompare => TSTOP_1,
            StopBit::OnDisable => TSTOP_2,
            StopBit::OnCompareAndDisable => TSTOP_3,
        }
    }
}

/// When start bit is enabled, configured shifters will output the contents of the start bit when the timer is
/// enabled and the timer counter will reload from the compare register on the first rising edge of the shift
/// clock.
#[derive(Copy, Clone)]
pub enum StartBit {
    /// 0b - Start bit disabled (default)
    Disabled,

    /// 1b - Start bit enabled
    Enabled,
}

impl StartBit {
    const fn tstart(self) -> u32 {
        use flexio::TIMCFG::TSTART::RW::*;
        match self {
            StartBit::Disabled => TSTART_0,
            StartBit::Enabled => TSTART_1,
        }
    }
}

macro_rules! daisy {
    ($nam:ident : $typ:ty) => {
        pub const fn $nam(self, $nam: $typ) -> Self {
            Self { $nam, ..self }
        }
    };
}

#[derive(Clone, Copy)]
pub struct Timer {
    pub mode: Mode,
    pub trigger: Trigger,
    pub pin: Pin,
    // TIMCFG fields:
    pub output: Output,
    pub decrement: Decrement,
    pub reset: Reset,
    pub disable: Disable,
    pub enable: Enable,
    pub stop_bit: StopBit,
    pub start_bit: StartBit,
}

impl Timer {
    pub const fn new(mode: Mode, trigger: Trigger, pin: Pin) -> Self {
        Self {
            mode,
            trigger,
            pin,
            output: Output::HighWhenEnabled,
            decrement: Decrement::Clock,
            reset: Reset::Never,
            disable: Disable::Never,
            enable: Enable::Always,
            stop_bit: StopBit::Disabled,
            start_bit: StartBit::Disabled,
        }
    }

    daisy!(output: Output);
    daisy!(decrement: Decrement);
    daisy!(reset: Reset);
    daisy!(disable: Disable);
    daisy!(enable: Enable);
    daisy!(stop_bit: StopBit);
    daisy!(start_bit: StartBit);

    pub fn apply<const N: u8>(&self, flexio: &flexio::Instance<N>, timer: usize) {
        // reset
        flexio.TIMCTL[timer].write(0);
        flexio.TIMCMP[timer].write(self.mode.timcmp());
        ral::write_reg!(
            ral::flexio,
            flexio,
            TIMCFG[timer],
            TIMOUT: self.output.timout(),
            TIMDEC: self.decrement.timdec(),
            TIMRST: self.reset.timrst(),
            TIMDIS: self.disable.timdis(),
            TIMENA: self.enable.timena(),
            TSTOP: self.stop_bit.tstop(),
            TSTART: self.start_bit.tstart(),
        );

        ral::write_reg!(
            ral::flexio,
            flexio,
            TIMCTL[timer],
            TRGSEL: self.trigger.trgsel(),
            TRGPOL: self.trigger.trgpol(),
            TRGSRC: self.trigger.trgsrc(),
            PINSEL: self.pin.pinsel(),
            PINCFG: self.pin.pincfg(),
            PINPOL: self.pin.pinpol(),
            TIMOD: self.mode.timod(),
        );
    }
}
