use bsp::{pins::imxrt_iomuxc as iomuxc, ral};

use crate::flexio::{timer, Polarity};

pub struct FlexIOConfig {
    pub shifter: u8,
    pub timer_gen: u8,
    pub timer_0: u8,
    pub timer_1: u8,
    pub pin_gen: u8,
    pub pin_clk: u8,
    pub pin_out: u8,
}

impl FlexIOConfig {
    pub const DEFAULT: Self = Self {
        shifter: 0,
        timer_gen: 0,
        timer_0: 1,
        timer_1: 2,
        pin_gen: 0,
        pin_clk: 1,
        pin_out: 2,
    };

    pub const fn for_pin<const N: u8, P: iomuxc::flexio::Pin<N>>(_pin: &P) -> Self {
        let o = P::OFFSET;
        let d = Self::DEFAULT;
        Self {
            pin_out: o,
            pin_clk: if d.pin_clk == o { d.pin_out } else { d.pin_clk },
            pin_gen: if d.pin_gen == o { d.pin_out } else { d.pin_gen },
            ..d
        }
    }

    pub fn apply<const N: u8>(self, flexio: &ral::flexio::Instance<N>) {
        // input timer is ~14.9MHz; TODO: calculate that in build time?
        pub(crate) const BIT_CLOCKS: u8 = 36; // T0H + T0L == T1H + T1L
        pub(crate) const T0H_CLOCKS: u8 = 8;
        pub(crate) const T0L_CLOCKS: u8 = 28;
        pub(crate) const T1H_CLOCKS: u8 = 26;
        pub(crate) const T1L_CLOCKS: u8 = 10;

        let shifter_gen: usize = self.shifter.into();

        // Shifter (TODO: flexio/shifter.rs)
        flexio.SHIFTCTL[shifter_gen].write(0); // reset
        ral::write_reg!(
            ral::flexio,
            flexio,
            SHIFTCFG[shifter_gen],
            SSTART: SSTART_1 // Start bit disabled, load data on first shift
        );
        ral::write_reg!(
            ral::flexio,
            flexio,
            SHIFTCTL[shifter_gen],
            TIMSEL: self.timer_gen as u32,
            TIMPOL: TIMPOL_0,           // Shift on positive edge of the timer
            PINCFG: PINCFG_3,           // output to a pin.
            PINSEL: self.pin_gen as u32, // Output start pin
            PINPOL: PINPOL_0,           // Pin polarity (active high)
            SMOD: SMOD_2,               // transmit mode
        );

        // gen timer
        timer::Timer::new(
            timer::Mode::Dual8BitBaud(32, BIT_CLOCKS),
            timer::Trigger::Shifter(self.shifter, Polarity::ActiveLow),
            timer::Pin::Output(self.pin_clk, Polarity::ActiveHigh),
        )
        .output(timer::Output::LowWhenEnabled)
        .enable(timer::Enable::Trigger)
        .disable(timer::Disable::Compare)
        .apply(flexio, self.timer_gen.into());

        // zero timer
        timer::Timer::new(
            timer::Mode::Dual8BitPWM(T0H_CLOCKS, T0L_CLOCKS),
            timer::Trigger::Pin(self.pin_clk, Polarity::ActiveHigh),
            timer::Pin::Output(self.pin_out, Polarity::ActiveHigh),
        )
        .enable(timer::Enable::TriggerRising)
        .disable(timer::Disable::Compare)
        .apply(flexio, self.timer_0 as _);

        // one timer
        timer::Timer::new(
            timer::Mode::Dual8BitPWM(T1H_CLOCKS, T1L_CLOCKS),
            timer::Trigger::Pin(self.pin_gen, Polarity::ActiveHigh),
            timer::Pin::Output(self.pin_out, Polarity::ActiveHigh),
        )
        .enable(timer::Enable::TriggerRising)
        .disable(timer::Disable::TriggerFalling)
        .apply(flexio, self.timer_1 as _);

        // TODO: use FlexIO timer for idle instead of sleeping before
        // frame start?
    }
}
