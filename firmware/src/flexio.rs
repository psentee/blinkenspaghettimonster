pub mod timer;

use imxrt_ral as ral;

#[derive(Copy, Clone)]
pub enum Polarity {
    ActiveHigh,
    ActiveLow,
}

pub fn reset<const N: u8>(flexio: &ral::flexio::Instance<N>) {
    ral::write_reg!(ral::flexio, flexio, CTRL, SWRST: SWRST_1);
    assert!(ral::read_reg!(ral::flexio, flexio, CTRL, SWRST == SWRST_1));
    ral::write_reg!(ral::flexio, flexio, CTRL, SWRST: SWRST_0);
    while ral::read_reg!(ral::flexio, flexio, CTRL, SWRST == SWRST_1) {
        core::hint::spin_loop();
    }
}

pub fn enable<const N: u8>(flexio: &ral::flexio::Instance<N>) {
    ral::write_reg!(ral::flexio, flexio, CTRL, FLEXEN: 1);
}
