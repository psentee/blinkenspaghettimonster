#![feature(const_maybe_uninit_zeroed)]
#![feature(const_maybe_uninit_assume_init)]
#![feature(const_mut_refs)]
#![feature(type_alias_impl_trait)]
#![feature(variant_count)]
#![no_std]
#![no_main]

use core::mem::MaybeUninit;

extern crate alloc;

#[cfg(feature = "calibrate")]
mod calibrator;

mod flexio;
mod logging;
mod timings;
mod trng;
mod util;
mod ws28xx;

const ARENA_SIZE: usize = 256 * 1024;

#[used]
#[link_section = ".uninit"]
static mut ARENA: MaybeUninit<[u8; ARENA_SIZE]> = MaybeUninit::zeroed();

#[global_allocator]
static ALLOCATOR: talc::Talck<spin::Mutex<()>, talc::InitOnOom> = talc::Talc::new(unsafe {
    talc::InitOnOom::new(talc::Span::from_slice(
        ARENA.assume_init_mut() as *const [u8] as *mut [u8],
    ))
})
.lock();

pub(crate) mod prelude {
    pub(crate) use alloc::boxed::Box;

    pub(crate) use bsp::board;
    pub(crate) use rand::rngs::SmallRng;
    pub(crate) use rand::{Rng, SeedableRng};
    // pub(crate) use rtic_core::prelude::*;
    pub(crate) use rtic_monotonics::systick::Systick;

    pub(crate) use spaghetti::{Coords, Frame};

    // Pre-parameterized types
    pub(crate) type FrameBuffer =
        spaghetti::FrameBuffer<{ Coords::N + 3 }, { spaghetti::framebuffer_words(Coords::N + 3) }>;

    pub(crate) use crate::logging::{debug_log, debug_value};
    pub(crate) use crate::timings::*;
}

#[rtic::app(
    device = bsp,
    peripherals = true,
    dispatchers = [CAN1, CAN2, CAN3, ENET, ENET2],
)]
mod app {
    use alloc::string::String;

    use crate::prelude::*;
    use crate::trng::Trng;

    type RenderedFrameBuffer = (Duration, Box<FrameBuffer>);

    type Ws28xx = crate::ws28xx::Ws28xx<
        2,                     // FLEXIO2
        2,                     // iomuxc::flexio::Pin<2> is implemented for P9
        bsp::pins::common::P9, // selected output pin
    >;

    const WS28XX_DMA_CHANNEL: usize = 2;

    #[shared]
    struct Shared {
        status: String,
    }

    #[local]
    struct Local {
        led: board::Led,
        pit: bsp::hal::pit::Pit<2>,
        poller: bsp::logging::Poller,
        ws28xx: Ws28xx,
        frame: Frame,
        frame_s: rtic_sync::channel::Sender<'static, RenderedFrameBuffer, 1>,
        frame_r: rtic_sync::channel::Receiver<'static, RenderedFrameBuffer, 1>,
        rng: SmallRng,
        // trng: Trng,
    }

    #[init]
    fn init(mut cx: init::Context) -> (Shared, Local) {
        cx.core.SCB.set_sleepdeep();

        // Initialize (enable) the monotonic timer (CYCCNT)
        cx.core.DCB.enable_trace();
        cx.core.DWT.enable_cycle_counter();

        let board::Resources {
            pins,
            mut gpio2,
            pit: (_, _, mut pit, _),
            usb,
            flexio2,
            mut dma,
            lpuart6,
            trng,
            ..
        } = board::t40(cx.device);

        let lpuart = board::lpuart(lpuart6, pins.p1, pins.p0, 115200);
        unsafe {
            // Safety: we're in a critical section, nothing can hurt us here.
            crate::logging::init(lpuart);
        }

        let poller = bsp::logging::log::usbd(usb, bsp::logging::Interrupts::Enabled).unwrap();

        // rtic_monotonics sets systick source to
        // `SystClkSource::Core`, which can be variable. We want to
        // use 100kHz `SystClkSource::External` provided by Teensy's
        // cristal oscillator
        let systick_token = rtic_monotonics::create_systick_token!();
        Systick::start(cx.core.SYST, bsp::EXT_SYSTICK_HZ, systick_token);
        {
            let mut systick = unsafe {
                // Safety: cx.core.SYST has been moved to Systick and
                // initialized to the wrong source. We're in the
                // critical sections and interrupts haven't started
                // yet, let's just quickly reconfigure it before
                // rtic_monotonics can notice it.
                core::mem::transmute::<(), cortex_m::peripheral::SYST>(())
            };
            systick.disable_counter();
            systick.set_clock_source(cortex_m::peripheral::syst::SystClkSource::External);
            systick.enable_counter();
        }

        let led = board::led(&mut gpio2, pins.p13);
        pit.set_interrupt_enable(true);
        pit.set_load_timer_value(DurationPERCLK::from_rate(FPS).ticks());
        pit.enable();

        let ws28xx = Ws28xx::prepare(flexio2, pins.p9, dma[WS28XX_DMA_CHANNEL].take().unwrap(), 1);

        fx::spawn().unwrap();

        let mut trng = Trng::new(trng);
        let mut rng = SmallRng::from_rng(&mut trng).unwrap();

        unsafe {
            debug_value(("trng,rng", trng.gen::<u16>(), rng.gen::<u16>()));
        }

        let (frame_s, frame_r) = rtic_sync::make_channel!(RenderedFrameBuffer, 1);

        (
            Shared {
                status: "Initialized".into(),
            },
            Local {
                led,
                pit,
                poller,
                ws28xx,
                frame: Frame::new(),
                frame_s,
                frame_r,
                rng,
                // trng,
            },
        )
    }

    #[idle]
    fn idle(_cx: idle::Context) -> ! {
        loop {
            // Allow MCU to sleep between interrupts
            rtic::export::wfi()
        }
    }

    #[task(
        binds = PIT,
        local = [
            led,
            pit,
            ctr: u32 = 0,
            max_duration: Duration = Duration::from_ticks(0),
        ],
    )]
    fn frame_timer(cx: frame_timer::Context) {
        let pit = cx.local.pit;
        while pit.is_elapsed() {
            pit.clear_elapsed();
        }

        #[cfg(not(feature = "calibrate"))]
        {
            if display::spawn().is_err() {
                log::trace!("Failed to spawn display");
            }
        }

        if *cx.local.ctr % FPS.raw() == 0 {
            // Do stuff every second
            cx.local.led.set();
            log::info!(
                "allocated {}",
                crate::ALLOCATOR.talc().get_allocated_span().size()
            );
        } else {
            cx.local.led.clear();
        }
        *cx.local.ctr += 1;
    }

    #[task(
        priority = 10,
        local = [
            ws28xx,
            frame_r,
            longest_frame: Duration = Duration::from_ticks(0),
            longest_frame_status: String = String::new(),
            last_log: Instant = Instant::from_ticks(0),
            frame_ctr: usize = 0,
        ],
        shared = [
            status,
        ],
    )]
    #[allow(unused_mut)] // mut is actually used
    async fn display(mut cx: display::Context) {
        let (dur, fb) = cx.local.frame_r.recv().await.unwrap();

        #[cfg(feature = "calibrate")] // silence unused warning
        let _ = dur;

        #[cfg(not(feature = "calibrate"))] // useless for calibrator
        if dur > *cx.local.longest_frame {
            *cx.local.longest_frame = dur;
            cx.shared.status.lock(|status| {
                *cx.local.longest_frame_status = status.clone();
            });
            log::info!(
                "New longest frame #{}: {}",
                cx.local.frame_ctr,
                dur.as_frame_percentage(),
            );
        } else if Duration::since(*cx.local.last_log) > Duration::secs_at_least(1) {
            cx.shared.status.lock(|status| {
                log::info!(
                    "Frame #{} computed in {}, longest was {} ({})",
                    cx.local.frame_ctr,
                    dur.as_frame_percentage(),
                    cx.local.longest_frame.as_frame_percentage(),
                    status,
                );
            });
            *cx.local.last_log = Instant::now();
        }

        let dma_start = Instant::now();
        // Duration::measure() doesn't seem to work well with .await
        if let Err(err) = cx.local.ws28xx.send(fb.words(), Systick {}).await {
            unsafe { debug_log("failed to send leds\r\n") };
            log::error!("Failed to send LEDs: {:?}", err);
        }
        let dma_duration = Duration::since(dma_start);

        log::trace!("DMA transfer took {}", dma_duration.as_frame_percentage());

        *cx.local.frame_ctr += 1;
    }

    #[task(
        priority = 5,
        local = [
            frame,
            frame_s,
            rng,
            started: Instant = Instant::from_ticks(0),
        ],
        shared = [
            status,
        ]
    )]
    async fn fx(mut cx: fx::Context) {
        type Duration = <Systick as rtic_monotonics::Monotonic>::Duration;
        Systick::delay(Duration::millis(2300)).await;

        *cx.local.started = Instant::now();

        #[cfg(feature = "calibrate")]
        crate::calibrator::calibrate(&mut cx).await;

        #[cfg(not(feature = "calibrate"))]
        spaghetti::fx::main(&mut cx).await;
    }

    #[async_trait::async_trait]
    impl spaghetti::Show for fx::Context<'_> {
        type Rng = SmallRng;

        async fn frame_done(&mut self) -> spaghetti::Hint {
            // Calibrator displays frames on demand, not on
            // timer. Regular fx has a timer wake the display task,
            // and then it pulls a frame.
            #[cfg(feature = "calibrate")]
            display::spawn().unwrap();

            // Process frame into pixels
            let mut fb = Box::new(FrameBuffer::new());
            fb.load_pixels(crate::util::iter_pixels(self.local.frame.iter()));

            // Send pixels to the display task
            self.local
                .frame_s
                .send((Duration::since(*self.local.started), fb))
                .await
                .map_err(|_| {})
                .unwrap();
            *self.local.started = Instant::now();

            spaghetti::Hint::PleaseContinue
        }

        fn frame_mut(&mut self) -> &mut spaghetti::Frame {
            self.local.frame
        }

        fn frame(&self) -> &spaghetti::Frame {
            self.local.frame
        }

        fn rng(&mut self) -> &mut SmallRng {
            self.local.rng
        }

        fn set_status(&mut self, status: String) {
            use rtic_core::prelude::*;

            log::info!("STATUS: {}", status);
            self.shared.status.lock(|shared_status| {
                *shared_status = status;
            });
        }

        async fn delay(duration: spaghetti::Duration) {
            Systick::delay(duration.convert()).await
        }
    }

    #[task(binds = USB_OTG1, local = [poller])]
    fn poll_logger(cx: poll_logger::Context) {
        cx.local.poller.poll();
    }

    #[task(binds = DMA2_DMA18, priority = 11)]
    fn dma2_complete(_cx: dma2_complete::Context) {
        unsafe {
            bsp::hal::dma::DMA.on_interrupt(2);
            bsp::hal::dma::DMA.on_interrupt(17);
        }
    }

    #[task(binds = DMA_ERROR, priority = 11)]
    fn dma_error(_cx: dma_error::Context) {
        unsafe {
            debug_log("DMA error\r\n");
        }
    }
}
