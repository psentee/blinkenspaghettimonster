use palette::Srgb;

pub fn linearize_color_grb(col: &Srgb) -> [u8; 3] {
    let lin: palette::LinSrgb<u8> = col.into_linear().into_format();
    [lin.green, lin.red, lin.blue]
}

#[allow(dead_code)]
pub fn linearize_color_rgb(col: &Srgb) -> [u8; 3] {
    let lin: palette::LinSrgb<u8> = col.into_linear().into_format();
    [lin.red, lin.green, lin.blue]
}

#[inline(always)]
pub fn linearize_color(col: &Srgb) -> [u8; 3] {
    #[cfg(not(feature = "chain"))]
    return linearize_color_grb(col);
    #[cfg(feature = "chain")]
    return linearize_color_rgb(col);
}

pub fn iter_pixels<'a>(frame: impl Iterator<Item = &'a Srgb>) -> impl Iterator<Item = [u8; 3]> {
    [
        // TODO: do something about status pixels
        spaghetti::colors::CORAL,
        spaghetti::colors::CORNFLOWERBLUE,
        spaghetti::colors::HONEYDEW,
    ]
    .iter()
    .map(crate::util::linearize_color_grb)
    .chain(frame.map(crate::util::linearize_color))
}
