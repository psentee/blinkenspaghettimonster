#!/usr/bin/env python3
import csv
from dataclasses import dataclass
from collections import Counter

# TODO: rotate 7.5° clockwise (some perspective tool would be good)

# UL: 158, 667
# UR: 2478, 317
# LL: 548, 2994
# LR: 2703, 2888


@dataclass
class LED:
    strip: int
    index: int
    x: int
    y: int


leds = []
with open("positions.csv") as cf:
    for s, i, x, y in csv.reader(cf):
        leds.append(LED(strip=int(s), index=int(i), x=int(x), y=int(y)))

for i in range(len(leds)):
    assert leds[i].strip * 60 + leds[i].index == i

x0 = min(led.x for led in leds)
y0 = min(led.y for led in leds)
x1 = max(led.x for led in leds) - x0
y1 = max(led.y for led in leds) - y0
xy1 = max((x1, y1))
# print("pub const LED_COORDS: [(u8, u8); crate::N_LEDS] = [")
# print(f"    // ({x0!r},{y0!r})+({x1},{y1})")

pairs = []
for led in leds:
    u8x = ((led.x - x0) / xy1)
    u8y = ((led.y - y0) / xy1)
    pairs.append(f"({u8x},{u8y})")
    # print(
    #     # f"{led.strip},{led.index},{u8x},{u8y}"
    #     f"({u8x}, {u8y}), // {led.strip * 60 + led.index} {led.strip}:{led.index}"
    # )

print(f"[{','.join(pairs)}]")
