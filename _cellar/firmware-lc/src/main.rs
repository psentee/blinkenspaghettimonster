#![no_std]
#![no_main]

// pick a panicking behavior
// extern crate panic_halt;
// extern crate panic_abort; // requires nightly
// extern crate panic_itm; // logs messages over ITM; requires ITM support
// extern crate panic_semihosting; // logs messages to the host stderr; requires a debugger
extern crate cichlid;
extern crate embedded_hal;

use cortex_m_rt::{entry, pre_init};

use spaghetti::prelude::*;

use teensy_lc::prelude::*;
use teensy_lc::sim::Copc;

mod ws2812b;
use ws2812b::WS2812B;

// C5 is LED
// B1 is 17 / 5V out

#[pre_init]
unsafe fn init() {
    teensy_lc::init();
}

#[entry]
fn main() -> ! {
    let pp = teensy_lc::dev::Peripherals::take().unwrap();
    let cpp = teensy_lc::dev::CorePeripherals::take().unwrap();

    // Disable watchdog
    Copc(&pp.SIM.copc).disable();

    // Init clock
    pp.MCG.constrain().init_clock_48mhz(&pp.SIM, &pp.OSC0);

    //// Turn LED on
    // Port C Clock Gate Control: enable clock to port B & C
    pp.SIM
        .scgc5
        .write(|w| w.portc().set_bit().portb().set_bit());

    let fgpioc = pp.FGPIOC.split();
    fgpioc.pc5.enable();
    let mut led_pin = fgpioc.pc5.into_output();
    led_pin.set_high().unwrap();

    let fgpiob = pp.FGPIOB.split();
    fgpiob.pb1.enable();
    // Enable pulldown on B1
    pp.PORTB.pcr1.modify(|_, w| w.pe().set_bit().ps()._0());
    let mut ws2812b_pin = fgpiob.pb1.into_output();
    ws2812b_pin.set_low().unwrap();
    teensy_lc::delay_usec(1_000_000);
    led_pin.set_low().unwrap();

    let mut frame = spaghetti::new_frame();

    let mut syst = cpp.SYST;
    let mut ws2812b = WS2812B::new(&mut ws2812b_pin, &mut syst);
    ws2812b.send(&frame);
    teensy_lc::delay_usec(1_000_000);

    let mut fx = spaghetti::main();

    let mut i: usize = 0;
    loop {
        fx.next(&mut frame);
        ws2812b.send(&frame);
        teensy_lc::delay_usec(10_000);

        // blink the heartbeat led
        i += 1;
        if i & 0x001f == 0 {
            led_pin.toggle();
        }
    }
}
