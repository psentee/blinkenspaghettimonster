mod player;
mod ui;

fn main() -> Result<(), eframe::Error> {
    env_logger::Builder::from_default_env()
        .format_timestamp_millis()
        .init();

    // https://stackoverflow.com/questions/35988775/how-can-i-cause-a-panic-on-a-thread-to-immediately-end-the-main-thread
    let orig_hook = std::panic::take_hook();
    std::panic::set_hook(Box::new(move |panic_info| {
        // invoke the default handler and exit the process
        orig_hook(panic_info);
        std::process::exit(1);
    }));

    eframe::run_native(
        "Spaghetti Player",
        Default::default(),
        Box::new(move |cc| Box::new(ui::App::new(player::Player::spawn(&cc.egui_ctx)))),
    )
}
