use spaghetti::{fx::Fx, Show};

#[derive(Copy, Clone, Debug, PartialEq)]
pub(crate) enum Playable {
    Fx(Fx),
    Main,
}

impl core::fmt::Display for Playable {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl Playable {
    pub fn all() -> impl Iterator<Item = Self> {
        [
            Self::Main,
            Self::Fx(Fx::Flood(0)),
            Self::Fx(Fx::Flood(60)),
            Self::Fx(Fx::Flood(90)),
            Self::Fx(Fx::Flood(180)),
            Self::Fx(Fx::Flood(270)),
        ]
        .into_iter()
        .chain(Fx::all().iter().cloned().map(Self::Fx))
    }

    pub async fn play<SH: Show>(&self, sh: &mut SH) {
        match self {
            Playable::Fx(fx) => {
                fx.play(sh).await;
            }
            Playable::Main => {
                spaghetti::fx::main(sh).await;
            }
        }
    }
}
