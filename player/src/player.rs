use std::sync::{Arc, Mutex};

use rand::{rngs::StdRng, SeedableRng};
use spaghetti::Frame;
use tokio::sync::mpsc;

mod playable;

pub(crate) use self::playable::Playable;

struct Show {
    // Show impl
    frame: Frame,
    rng: StdRng,
    sender: mpsc::Sender<Frame>,
    status: String,
}

#[async_trait::async_trait]
impl spaghetti::Show for Show {
    type Rng = StdRng;

    async fn frame_done(&mut self) -> spaghetti::Hint {
        // Send frame to rendering (copying it)
        self.sender.send(self.frame).await.unwrap();
        spaghetti::Hint::PleaseContinue
    }

    fn frame_mut(&mut self) -> &mut Frame {
        &mut self.frame
    }

    fn frame(&self) -> &Frame {
        &self.frame
    }

    fn rng(&mut self) -> &mut StdRng {
        &mut self.rng
    }

    fn set_status(&mut self, status: String) {
        self.status = status;
    }

    async fn delay(duration: spaghetti::Duration) {
        tokio::time::sleep(tokio::time::Duration::from_millis(
            duration.to_millis().into(),
        ))
        .await
    }
}

#[derive(Clone, Copy)]
enum Command {
    Play(Option<Playable>), // None means stop
    SetFrameRate(f64),      // 0.0 means paused
    Skip(usize),
}

/// This struct is a field of Ui, sends commands across the sync/async boundary to Performer, and receives a Frame
pub(crate) struct Player {
    frame: Arc<Mutex<(Frame, usize)>>,
    cmd: mpsc::Sender<Command>,
    fps: f64,
    pble: Option<Playable>,
}

impl Player {
    const INITIAL_FPS: f64 = spaghetti::FPS.to_Hz() as f64;

    pub(crate) fn spawn(ctx: &eframe::egui::Context) -> Self {
        let (cmd, cmd_r) = mpsc::channel::<Command>(1);
        let frame = Arc::new(Mutex::new((Frame::new(), 0usize)));
        let ctx = ctx.clone();
        let frame2 = frame.clone();
        std::thread::spawn(move || {
            let rt = tokio::runtime::Builder::new_current_thread()
                .enable_all()
                .build()
                .unwrap();
            rt.block_on(Self::perform(frame2, cmd_r, &ctx));
        });

        Self {
            frame,
            cmd,
            fps: Self::INITIAL_FPS,
            pble: None,
        }
    }
    async fn perform(
        frame: Arc<Mutex<(Frame, usize)>>,
        mut cmd: mpsc::Receiver<Command>,
        ctx: &eframe::egui::Context,
    ) -> ! {
        use futures::future::{select, Either, FutureExt};

        let mut fx_task: Option<tokio::task::JoinHandle<()>> = None;

        let mut frame_rate =
            tokio::time::interval(core::time::Duration::from_secs_f64(1.0 / Self::INITIAL_FPS));

        let (frame_s, mut frame_r) = mpsc::channel::<Frame>(1);
        let (skip_s, mut skip_r) = mpsc::channel::<usize>(1);

        loop {
            let cmd = if fx_task.is_none() {
                // when nothing is playing we don't need to wait on frame
                if let Some(cmd) = cmd.recv().await {
                    Either::Right(cmd)
                } else {
                    continue;
                }
            } else {
                match select(
                    async {
                        let n: usize = match select(
                            frame_rate.tick().boxed_local(),
                            skip_r.recv().boxed_local(),
                        )
                        .await
                        {
                            Either::Right((Some(n), _)) => n,
                            _ => 0,
                        };

                        for _ in 0..n {
                            frame_r.recv().await;
                        }
                        // FIXME: if we get cmd after frame_rate.tick()
                        // but before receiving a frame, we're losing a
                        // frame.
                        frame_r.recv().await
                    }
                    .boxed_local(),
                    cmd.recv().boxed_local(),
                )
                .await
                {
                    Either::Left((Some(new_frame), _)) => Either::Left(new_frame),
                    Either::Right((Some(cmd), _)) => Either::Right(cmd),
                    _ => {
                        continue;
                    }
                }
            };

            match cmd {
                Either::Left(new_frame) => {
                    {
                        let mut fr = frame.lock().unwrap();
                        fr.0 = new_frame;
                        fr.1 += 1;
                    }
                    ctx.request_repaint();
                }
                Either::Right(Command::Play(pble)) => {
                    if let Some(jh) = fx_task.take() {
                        jh.abort();
                    }

                    while frame_r.try_recv().is_ok() {}

                    {
                        let mut fr = frame.lock().unwrap();
                        fr.0.clear();
                        fr.1 = 0;
                    }

                    ctx.request_repaint();

                    if let Some(pble) = pble {
                        let mut show = Show {
                            frame: Frame::new(),
                            rng: StdRng::from_entropy(),
                            sender: frame_s.clone(),
                            status: String::new(),
                        };
                        fx_task = Some(tokio::task::spawn(async move {
                            pble.play(&mut show).await;
                        }));
                    }
                }
                Either::Right(Command::SetFrameRate(fr)) => {
                    frame_rate = tokio::time::interval(if fr > 0. {
                        core::time::Duration::from_secs_f64(1.0 / fr)
                    } else {
                        // <=0 means paused, let's play at a bit below 1 FPY
                        core::time::Duration::from_secs(3600 * 24 * 366)
                    });
                }
                Either::Right(Command::Skip(n)) => {
                    let n = n.saturating_sub(1);
                    {
                        let mut fr = frame.lock().unwrap();
                        fr.1 += n;
                    }
                    skip_s.send(n).await.unwrap();
                }
            }
        }
    }

    pub(crate) fn skip(&mut self, n: usize) {
        self.cmd.blocking_send(Command::Skip(n)).unwrap();
    }

    fn play_(&self) {
        self.cmd.blocking_send(Command::Play(self.pble)).unwrap();
    }

    pub(crate) fn play(&mut self, pble: Playable) {
        self.pble = Some(pble);
        self.play_();
    }

    pub(crate) fn stop(&mut self) {
        self.pble = None;
        self.play_();
    }

    pub(crate) fn frame(&self) -> Frame {
        // FIXME: this also clones all the pixels
        self.frame.lock().unwrap().0
    }

    pub(crate) fn frame_number(&self) -> usize {
        self.frame.lock().unwrap().1
    }

    pub(crate) fn paused(&self) -> bool {
        self.fps <= 0.0
    }

    #[allow(dead_code)]
    pub(crate) fn set_paused(&mut self, paused: bool) {
        self.set_fps(self.fps() * if paused { -1.0 } else { 1.0 });
    }

    pub(crate) fn toggle_pause(&mut self) {
        self.set_fps(-self.fps);
    }

    pub(crate) fn fps(&self) -> f64 {
        self.fps.abs()
    }

    pub(crate) fn set_fps(&mut self, fps: f64) {
        self.fps = fps;
        self.cmd.blocking_send(Command::SetFrameRate(fps)).unwrap();
    }

    pub(crate) fn stopped(&self) -> bool {
        self.pble.is_none()
    }
}
