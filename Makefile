BIN:=	spaghetti-firmware

FEATURES?=	

.CARGOFLAGS:=	
.if $(FEATURES)
.CARGOFLAGS+=	--features $(FEATURES)
.endif

OUT_DIR=	./target/thumbv7em-none-eabihf/release
OUT_ELF=	${OUT_DIR}/${BIN}
OUT_HEX=	${OUT_DIR}/${BIN}.hex
OUT_BIN=	${OUT_DIR}/${BIN}.bin

FLASH=	teensy-loader-cli -v -w --mcu=TEENSY40
CARGO=	cargo -Z unstable-options

.MAIN: size ${OUT_HEX}
all: ${OUT_HEX}
bin: ${OUT_BIN}
elf: ${OUT_ELF}

${OUT_BIN}: ${OUT_ELF}
	objcopy -O binary ${OUT_ELF} $@

${OUT_HEX}: ${OUT_ELF}
	objcopy -O ihex ${OUT_ELF} $@

${OUT_ELF}: build

.PHONY: build
build:
	$(CARGO) -C firmware build --release ${.CARGOFLAGS}
.ifdef NO_CHECK
	-./scripts/check-firmware.sh $(OUT_ELF)
.else
	./scripts/check-firmware.sh $(OUT_ELF)
.endif

.PHONY: size
size: ${OUT_ELF}
	size -A ${OUT_ELF}

.PHONY: flash fl
fl flash: ${OUT_HEX}
	${FLASH} ${OUT_HEX}
	$(MAKE) log

SERIAL?=	/dev/serial/by-id/usb-5824_imxrt-log-if00
.PHONY: log ulog
log:
	while ! test -e $(SERIAL) ; do sleep 0.0001 ; done
	picocom -q -b 115200 $(SERIAL)

ulog:
	$(MAKE) log SERIAL=/dev/serial/by-id/usb-1a86_USB2.0-Serial-if00-port0


# flash.blinky:
# 	${FLASH} ./vendor/teensy_loader_cli/blink_slow_TeensyLC.hex

.PHONY: repl
repl:
	nix repl --file ./repl.nix

.PHONY: play pl
play pl:
	$(CARGO) -C player run $(.CARGOFLAGS)

.PHONY: clean autoclave
clean:
	rm -rf target
autoclave:
	git clean -fdx

.PHONY: c cp cc ca cl clc clp cla
c:
	$(CARGO) -C firmware check $(.CARGOFLAGS)

cc:
	$(CARGO) -C firmware check --features calibrate $(.CARGOFLAGS)

cp:
	$(CARGO) -C player check $(.CARGOFLAGS)

ca: c cc cp

cl:
	$(CARGO) -C firmware clippy $(.CARGOFLAGS)

clc:
	$(CARGO) -C firmware clippy --features calibrate $(.CARGOFLAGS)

clp:
	$(CARGO) -C player clippy $(.CARGOFLAGS)

cla: cl clc clp

.PHONY: doc.rust doc.std doc.core
doc.rust:
	xdg-open "$$(rustc --print sysroot)/share/doc/rust/html/$(COMPONENT)$(COMPONENT:D/)index.html"
doc.std:
	$(MAKE) doc.rust COMPONENT=std
doc.core:
	$(MAKE) doc.rust COMPONENT=core
